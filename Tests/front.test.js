require('chromedriver');

const {Builder, By, Key, until} = require('selenium-webdriver');
var webdriver = require('selenium-webdriver')
var assert = require('chai').assert;

describe('Login', function() {
    this.timeout(30000)
    let driver
    let vars
    beforeEach(async function() {
      driver = await new Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
    })
    afterEach(async function() {
      await driver.quit();
    })
    it('LoginSuccess', async function() {
      await driver.get("http://localhost:4200/")
      await driver.manage().window().setRect({ width: 1050, height: 708 })
      await driver.findElement(By.xpath("//div[@id=\'root\']/div/form/div/input")).click()
      await driver.findElement(By.xpath("//div[@id=\'root\']/div/form/div/input")).sendKeys("arielmacario.11@gmail.com")
      await driver.findElement(By.xpath("//input[@id=\'formBasicPassword\']")).click()
      await driver.findElement(By.xpath("//div[@id=\'root\']/div/form/div[2]/input")).sendKeys("password123")
      await driver.findElement(By.css(".btn-primary")).click()
    })

    it('LoginFailed', async function() {
        await driver.get("http://localhost:4200/")
        await driver.manage().window().setRect({ width: 1050, height: 708 })
        await driver.findElement(By.id("formBasicEmail")).click()
        await driver.findElement(By.id("formBasicEmail")).sendKeys("arielmacario.11@gmail.com")
        await driver.findElement(By.css(".mb-3:nth-child(2)")).click()
        await driver.findElement(By.id("formBasicPassword")).click()
        await driver.findElement(By.id("formBasicPassword")).sendKeys("123")
        await driver.findElement(By.css(".btn-primary")).click()
        await driver.close()
      })
  })
  


  describe('Registro', function() {
    this.timeout(30000)
    let driver
    let vars
    beforeEach(async function() {
        driver = await new Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
    })
    afterEach(async function() {
      await driver.quit();
    })
    it('RegistroSuccess', async function() {
      await driver.get("http://localhost:4200/Registro")
      await driver.manage().window().setRect({ width: 1050, height: 708 })
      await driver.findElement(By.id("formBasicEmail")).click()
      await driver.findElement(By.id("formBasicEmail")).sendKeys("Ariel")
      await driver.findElement(By.id("formBasicPassword")).sendKeys("Macario|")
      await driver.findElement(By.id("formBasicPassword")).click()
      await driver.findElement(By.id("formBasicPassword")).sendKeys("Macario")
      await driver.findElement(By.name("Email")).click()
      await driver.findElement(By.name("Email")).sendKeys("arielmacario.11@gmail.com")
      await driver.findElement(By.name("FechaNacimiento")).click()
      await driver.findElement(By.name("FechaNacimiento")).click()
      await driver.findElement(By.name("FechaNacimiento")).sendKeys("2022-06-02")
      await driver.findElement(By.name("Password")).click()
      await driver.findElement(By.name("Password")).sendKeys("123")
      await driver.findElement(By.name("RePassword")).click()
      await driver.findElement(By.name("RePassword")).sendKeys("123")
      await driver.findElement(By.css(".btn-primary")).click()
    })

    it('RegistroFailed', async function() {
        await driver.get("http://localhost:4200/Registro")
        await driver.manage().window().setRect({ width: 1050, height: 708 })
        await driver.findElement(By.id("formBasicEmail")).click()
        await driver.findElement(By.id("formBasicEmail")).sendKeys("Ariel ")
        await driver.findElement(By.id("formBasicPassword")).click()
        await driver.findElement(By.id("formBasicPassword")).sendKeys("Macario")
        await driver.findElement(By.name("FechaNacimiento")).click()
        await driver.findElement(By.name("FechaNacimiento")).sendKeys("2022-06-10")
        await driver.findElement(By.name("Password")).click()
        await driver.findElement(By.name("Password")).sendKeys("123")
        await driver.findElement(By.name("RePassword")).click()
        await driver.findElement(By.name("RePassword")).sendKeys("321")
        await driver.findElement(By.css(".btn-primary")).click()
      })
  })
  

  describe('AsignarSuccess', function() {
    this.timeout(30000)
    let driver
    let vars
    beforeEach(async function() {
        driver = await new Builder().withCapabilities(webdriver.Capabilities.chrome()).build()
    })
    afterEach(async function() {
      await driver.quit();
    })
    it('AsignarSuccess', async function() {
      await driver.get("http://localhost:4200/Asignar")
      await driver.manage().window().setRect({ width: 1050, height: 708 })
      await driver.findElement(By.id("formBasicEmail")).click()
      await driver.findElement(By.id("formBasicEmail")).sendKeys("1")
      await driver.findElement(By.id("formBasicPassword")).click()
      await driver.findElement(By.id("formBasicPassword")).sendKeys("Ariel Macario")
      await driver.findElement(By.name("Curso")).sendKeys("Fisica 1")
      await driver.findElement(By.name("Seccion")).sendKeys("A")
      await driver.findElement(By.name("Dia")).click()
      await driver.findElement(By.name("Dia")).sendKeys("Lunes")
      await driver.findElement(By.name("Hora")).sendKeys("7:00")
      await driver.findElement(By.css(".btn-primary")).click()
    })

    it('AsignarFailed', async function() {
        await driver.get("http://localhost:4200/Asignar")
        await driver.manage().window().setRect({ width: 1050, height: 708 })
        await driver.findElement(By.id("formBasicEmail")).click()
        await driver.findElement(By.id("formBasicEmail")).sendKeys("1")
        await driver.findElement(By.id("formBasicPassword")).click()
        await driver.findElement(By.id("formBasicPassword")).sendKeys("Ariel Macario")
        await driver.findElement(By.name("Curso")).sendKeys("Religion")
        await driver.findElement(By.name("Seccion")).sendKeys("A")
        await driver.findElement(By.name("Dia")).click()
        await driver.findElement(By.name("Dia")).sendKeys("Lunes")
        await driver.findElement(By.name("Hora")).sendKeys("7:00")
        await driver.findElement(By.css(".btn-primary")).click()
      })
  })