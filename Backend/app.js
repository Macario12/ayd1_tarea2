const express = require('express')
const bodyParser = require('body-parser')

const port = 3000
const app = express()

app.use(express.json());

app.use((req,res,next) =>{
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, OPTIONS ,POST, PUT, DELETE');

  next();

});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send("Servidor en el puerto 3000")
})

//Registro Estudiante
 app.post('/registrar', (req, res) => {
    const {Id,Nombre,Apellido,Email,FechaNacimiento,Password,RePassword} = req.body;
    if (Id && Nombre && Apellido && Email && FechaNacimiento && Password && RePassword){
        res.status(201).json("Estudiante registrado correctamente")
    }else{
        res.status(400).json("Estudiante no registrado")
    }

    
    
  })

  app.post('/login', (req, res) => {
    const {Email,Password} = req.body;
    if (Email && Password){
        if(Email == "arielmacario.11@gmail.com" && Password == "password123"){
          return res.status(202).json("Inicio de sesion exitoso")
        }

        return res.status(401).json("Inicio de sesion no exitoso")
        
    }

    res.status(400).json("Parametros no enviados")
    
  })

  app.post('/asignar', (req, res) => {
    const {Id,Estudiante,Curso, Seccion, Dia, Hora} = req.body;
    if (Id && Estudiante && Curso && Seccion && Dia && Hora){
      if(Curso == "Fisica 1"){
        return res.status(201).json("Asignacion correctamente")
      }else{
        return res.status(404).json("Curso no encontrado")
      }
        
    }

    res.status(400).json("Falta de datos, no se pudo asignar")
    
  })

app.listen(port, () => {
    
  console.log(`Example app listening on port ${port}`)
})


module.exports = app;