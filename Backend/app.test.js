const request = require('supertest')

const app = require('./app')
//Referencia de codigos de respuesta.
//https://kinsta.com/es/blog/codigos-de-estado-de-http/ 

describe("POST /login", ()=>{
    //Envio de parametros correctos
    it("Responde exitosamente", (done)=>{
        const data = {
            Email: "arielmacario.11@gmail.com",
            Password: "password123"
        };
        request(app)
        .post("/login")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(202)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
    //Envio de parametros erroneos
    it("Responde con 400, ya que no se enviaron las credenciales", (done)=>{
        const data = {
        };
        request(app)
        .post("/login")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(400)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })

     //Envio de parametros erroneos y esperando respuesta correcta
    it("Responde con 401, ya que el usuario no fue encontrado", (done)=>{
        const data = {
            Email: "arielmacariio.11@gmail.com",
            Password: "password123"
        };
        request(app)
        .post("/login")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(401)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
})



describe("POST /registrar", ()=>{
    //Envio de parametros correctos
    it("Responde exitosamente", (done)=>{
        const data = {
            Id: 45,
            Nombre: "Ariel",
            Apellido: "Macario",
            Email: "arielmacario.11@gmail.com",
            FechaNacimiento: "19/05/2001",
            Password: "password123",
            RePassword: "password123"
        };
        request(app)
        .post("/registrar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(201)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
    //Envio de parametros erroneos
    it("Responde con 400, ya que no se enviaron los parametros correctos", (done)=>{
        const data = {
        };
        request(app)
        .post("/registrar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(400)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })

     //Envio de parametros erroneos y esperando respuesta correcta
    it("Responde con la respuesta esperada", (done)=>{
        const data = {
            Id: 45,
            Nombre: "Ariel",
            Apellido: "Macario",
            Email: "arielmacario.11@gmail.com",
            FechaNacimiento: "19/05/2001",
            Password: "password123",
            RePassword: "password123"
        };
        request(app)
        .post("/registrar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
       // .expect(201)
        .expect('"Estudiante registrado correctamente"')
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
})

describe("POST /asignar", ()=>{
    //Envio de parametros correctos
    it("Responde exitosamente", (done)=>{
        const data = {
            Id: 45,
            Estudiante: "Ariel Macario",
            Curso: "Fisica 1",
            Seccion: "B+",
            Dia: "Lunes",
            Hora: "9:00"
        };
        request(app)
        .post("/asignar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(201)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
    //Envio de parametros erroneos
    it("Responde con 400, ya que no se enviaron los parametros correctos", (done)=>{
        const data = {
        };
        request(app)
        .post("/asignar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(400)
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })

     //Envio de parametros erroneos y esperando respuesta correcta
    it("Responde con la respuesta esperada", (done)=>{
        const data = {
            Id: 45,
            Estudiante: "Ariel Macario",
            Curso: "Fisica 1+",
            Seccion: "B+",
            Dia: "Lunes",
            Hora: "9:00"
        };
        request(app)
        .post("/asignar")
        .set("Accept", "application/json")
        .expect("Content-Type", /json/)
        .send(data)
        .expect(404)
        
        .end((err) => {
            if (err) return done(err);
            done();
        });

    })
})