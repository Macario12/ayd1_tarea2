import React, {  useState } from "react";
import { useNavigate } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import { Button,Alert } from 'react-bootstrap'

//Import para backend
import { helpHttp } from "../helper/helper";
import { Global } from "../helper/Global";

export default function Registrar() {
    let api = helpHttp();
    let navigate = useNavigate();

    
    const url = Global.url;
    const [Error, setError] = useState({
      Activar: false,
      Mensaje: '',
      Variant: ''
    });
    const [formulario, setFormulario] = useState({
        Id: 5,
        Nombre: '',
        Apellido: '',
        Email: '',
        FechaNacimiento: '',
        Password: '',
        RePassword: '',
      });

    const enviarFormulario = ()=>{
        let urlTerceros = url + "registrar";
            api
              .post(urlTerceros, {
                body: formulario
                ,
              })
              .then((res) => {
                if (!res.err) {
                  //setError(res)S
                  console.log(res)
                  setError({
                    Activar: true,
                    Mensaje: 'Exito, se a asignado el usuario.',
                    Variant: 'success'
                  }
                    );

                } else {
                  setError({
                    Activar: true,
                    Mensaje: 'Error, no se registro el usuario',
                    Variant: 'danger'
                  }
                    );
                  console.log("Error no se pudo realizar la peticion");
                }
              });
    }

    const handleInputChange = (e) => {
        setFormulario({
          ...formulario,
          [e.target.name]: e.target.value,
        });
      };

  return (
  <div>
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Nombre</Form.Label>
        <Form.Control type="text" placeholder="Nombre" name="Nombre" onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Apellido</Form.Label>
        <Form.Control type="text" placeholder="Apellido" name="Apellido"  onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="Email" name="Email" onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Fecha de Nacimiento</Form.Label>
        <Form.Control type="date" placeholder="Fecha de Nacimiento" name="FechaNacimiento"  onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" name="Password" onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Confirmar Password</Form.Label>
        <Form.Control type="password" placeholder="Confirmar Password" name="RePassword"  onChange={handleInputChange}/>
      </Form.Group>
      <Button variant="primary" onClick={enviarFormulario}>
        Ingresar
      </Button>
      <Button variant="secondary" href="/">Regresar.</Button>
    </Form>
    <br/>
    {Error.Activar ? (
        <Alert key={0} variant={Error.Variant}>
          {Error.Mensaje}
        </Alert>
      ) : null}
  </div>
  )
}
