import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Form from "react-bootstrap/Form";
import { Button, Alert } from "react-bootstrap";

//Import para backend
import { helpHttp } from "../helper/helper";
import { Global } from "../helper/Global";

export default function Asignar() {
  let api = helpHttp();
  let navigate = useNavigate();

  const url = Global.url;
  const [Error, setError] = useState({
    Activar: false,
    Mensaje: '',
    Variant: ''
  });
  const [formulario, setFormulario] = useState({
    Id: "",
    Estudiante: "",
    Curso: "",
    Seccion: "",
    Dia: "",
    Hora: "",
  });

  const enviarFormulario = () => {
    let urlTerceros = url + "asignar";
    api
      .post(urlTerceros, {
        body: formulario,
      })
      .then((res) => {
        if (!res.err) {
          //setError(res)S
          console.log(res);
          setError({
            Activar: true,
            Mensaje: 'Exito, te has asignado a un curso.',
            Variant: 'success'
          }
            );
        } else {
          setError({
            Activar: true,
            Mensaje: 'Error, no se a realizado la asignacion',
            Variant: 'danger'
          }
            );
          console.log("Error no se pudo realizar la peticion");
        }
      });
  };

  const handleInputChange = (e) => {
    setFormulario({
      ...formulario,
      [e.target.name]: e.target.value,
    });
  };

  return (
    <div>
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Id</Form.Label>
          <Form.Control
            type="text"
            placeholder="Id"
            name="Id"
            onChange={handleInputChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Estudiante</Form.Label>
          <Form.Control
            type="text"
            placeholder="Estudiante"
            name="Estudiante"
            onChange={handleInputChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Curso</Form.Label>
          <Form.Control
            type="text"
            placeholder="Curso"
            name="Curso"
            onChange={handleInputChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Seccion</Form.Label>
          <Form.Control
            type="text"
            placeholder="Seccion"
            name="Seccion"
            onChange={handleInputChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Dia</Form.Label>
          <Form.Control
            type="text"
            placeholder="Dia"
            name="Dia"
            onChange={handleInputChange}
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Hora</Form.Label>
          <Form.Control
            type="text"
            placeholder="Hora"
            name="Hora"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Button variant="primary" onClick={enviarFormulario}>
          Asignar
        </Button>
        <Button variant="secondary" href="/">Cerrar Sesion.</Button>
      </Form>
      <br />
      {Error.Activar ? (
        <Alert key={0} variant={Error.Variant}>
          {Error.Mensaje}
        </Alert>
      ) : null}
    </div>
  );
}
