import React, {  useState } from "react";
import { useNavigate } from "react-router-dom";
import Form from 'react-bootstrap/Form'
import { Button,Alert } from 'react-bootstrap'

//Import para backend
import { helpHttp } from "../helper/helper";
import { Global } from "../helper/Global";

export default function Login() {

    let api = helpHttp();
    let navigate = useNavigate();

    
    const url = Global.url;
    const [Error, setError] = useState(false);
    const [formulario, setFormulario] = useState({
        Email: '',
        Password: ''
      });

    const enviarFormulario = ()=>{
        let urlTerceros = url + "login";
            api
              .post(urlTerceros, {
                body: formulario
                ,
              })
              .then((res) => {
                if (!res.err) {
                  //setError(res)S
                  console.log(res)
                  return navigate("/Asignar");

                } else {
                    setError(true);
                  console.log("Error no se pudo realizar la peticion");
                }
              });
    }

    const handleInputChange = (e) => {
        setFormulario({
          ...formulario,
          [e.target.name]: e.target.value,
        });
      };

  return (
    <div>
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Usuario o Correo Electronico</Form.Label>
        <Form.Control type="email" placeholder="maca" name="Email" onChange={handleInputChange}/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Contraseña</Form.Label>
        <Form.Control type="password" placeholder="Contraseña" name="Password"  onChange={handleInputChange}/>
      </Form.Group>
      <Button variant="primary" onClick={enviarFormulario}>
        Ingresar
      </Button>
      <Button variant="link" href="/Registro">Registrate, para obtener un perfil.</Button>
    </Form>
    <br/>
    {Error ? <Alert key={'danger'} variant={'danger'}>
    Error datos erroneos
  </Alert> : null}
  </div>
  )
}
